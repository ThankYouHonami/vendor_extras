# Overlay
PRODUCT_PACKAGES += \
    CertifiedPropsOverlay \
    POSFrameworks

PRODUCT_COPY_FILES += \
    $(LOCAL_PATH)/configs/config-system_ext.xml:$(TARGET_COPY_OUT_SYSTEM_EXT)/overlay/config/config.xml
